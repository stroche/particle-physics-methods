# RooFit

![roofit-example](https://web.pa.msu.edu/people/brock/file_sharing/ATLAS/root/roofit/doc/v524/roofit_524_visualerror.gif)

RooFit is a package built into ROOT that is both very useful and used in many analyses. RooFit provides statistical modeling of event distributions, complex fits, and statistical tools common in particle physics.

Proceed through this tutorial in the following order:
1. [Gaussian fit](gaussian-fit)
2. [Custom PDF](custom-pdf)
3. [Bump hunt](bump-hunt)
4. [Data/Monte Carlo fit](data-mc)
