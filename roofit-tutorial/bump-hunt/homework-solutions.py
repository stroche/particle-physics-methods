import ROOT
import numpy as np

#--------------------------------
# fetch data
#--------------------------------
data_values = np.genfromtxt("homework-dataset.csv")

#--------------------------------
# make model
#-------------------------------
# this is the distribution we'll be looking at
mass = ROOT.RooRealVar("mass", "mass", 0, 200)

# background variables
c = ROOT.RooRealVar("c", "c", -0.1, -1, 0)

# signal variables
# we want to find a particle in the 100 --> 150 GeV range
mean = ROOT.RooRealVar("mean", "mean",    100, 100, 150)
sigma = ROOT.RooRealVar("sigma", "sigma", 1, 0, 40)


# what fraction is signal
# initial guess = half
fsig = ROOT.RooRealVar("fsig", "fsig", 0.5, 0, 1)

# background
sig = ROOT.RooGaussian("sig", "sig",
                       mass, mean, sigma)
# background
bkg = ROOT.RooExponential("bkg", "bkg",
                          mass, c)

# total model is the sum of those
model = ROOT.RooAddPdf("model", "model",
                       ROOT.RooArgList(sig, bkg), fsig)

#------------------------------
# fit model to data
#------------------------------
data = ROOT.RooDataSet.from_numpy({'mass': data_values}, [mass])
model.fitTo(data)

# print values
print(mean)
print(sigma)
print(c)
print(fsig)


#-------------------------------
# plot results
#-------------------------------
hist_data = ROOT.TH1F("data_hist", "", 50, 0, 200)
for v in data_values:
    hist_data.Fill(v)

# plot bkg only, sig only, and combo
bkg_only_graph = ROOT.TGraph()
sig_only_graph = ROOT.TGraph()
model_graph =    ROOT.TGraph()

for v in np.linspace(0, 200, 1000):
    mass.setVal(v)
    n = bkg_only_graph.GetN()
    
    bkg_only_graph.SetPoint(n, v, bkg.getVal(mass))
    sig_only_graph.SetPoint(n, v, sig.getVal(mass))
    model_graph.SetPoint(   n, v, model.getVal(mass))


bkg_only_graph.Scale(hist_data.GetBinWidth(1)*len(data_values)*(1.0-fsig.getVal()))
sig_only_graph.Scale(hist_data.GetBinWidth(1)*len(data_values)*fsig.getVal())
model_graph.Scale(hist_data.GetBinWidth(1)*len(data_values))


leg = ROOT.TLegend(0.6, 0.6, 0.88, 0.88)
leg.AddEntry(hist_data, "Data", "lep")
leg.AddEntry(bkg_only_graph, "Background only", "l")
leg.AddEntry(sig_only_graph, "Signal only", "l")
leg.AddEntry(model_graph, "Combined model", "l")


hist_data.GetXaxis().SetTitle("Mass [GeV]")
hist_data.GetYaxis().SetTitle("Events / 4 GeV")
hist_data.SetStats(0)
hist_data.SetLineWidth(2)

bkg_only_graph.SetLineColor(ROOT.kRed)
sig_only_graph.SetLineColor(ROOT.kMagenta)
model_graph.SetLineColor(ROOT.kBlack)


c = ROOT.TCanvas()
hist_data.Draw("e0")
bkg_only_graph.Draw("c, same")
sig_only_graph.Draw("c, same")
model_graph.Draw("c, same")

leg.Draw("same")
c.SaveAs("homework-solutions.png")
