import ROOT
import numpy as np
from scipy.stats import norm

#--------------------------------------
# function definitions
#-------------------------------------
def gaussian(x, mu, sig):
    return (1.0 / (sig * np.sqrt(2*np.pi))) * np.exp(-0.5 * ((x-mu)/sigma)**2.0)

def normalize(hist):
    integral = 0.0
    for i in range(hist.GetNbinsX()+1):
        integral += hist.GetBinContent(i) * hist.GetBinWidth(i)
    hist.Scale(1.0 / integral)
    return hist

#---------------------------------------
# data generation
#---------------------------------------
# make some gaussian data
# mean of 3.2, std dev of 1.3
mean = 3.2
sigma = 1.3
gaussian_data = np.random.normal(loc=mean, scale=sigma, size = 10000)

#---------------------------------------
# RooFit model setup
#---------------------------------------

# define RooFit variables
# RooRealVar means it's a real variable
x_var = ROOT.RooRealVar("x", "x", -20, 20) # define the variable x bounded between -20 and 20
mean_var = ROOT.RooRealVar("mean", "mean of gaussian", 0, -20, 20) # guess that the mean is 0 (bound between -20 and 20)
sigma_var = ROOT.RooRealVar("sigma", "std of gaussian", 1, 0, 10) # guess that the standard deviation is 1 (bounded between -5 and 5)

# create gaussian PDF using these variables
gauss = ROOT.RooGaussian("gauss", "gaussianPDF", x_var, mean_var, sigma_var)

#---------------------------------------
# fit model to data
#---------------------------------------
# make a RooDataSet from the random gaussian set from earlier
data = ROOT.RooDataSet.from_numpy({'x':gaussian_data}, [x_var])
gauss.fitTo(data)

roofit_mean = gauss.getMean().getVal()
roofit_sigma = gauss.getSigma().getVal()
#---------------------------------------
# print results
# --------------------------------------

print('True mean: %s    RooFit estimated mean: %s' % (mean, roofit_mean))
print('True sigma: %s   RooFit estimated sigma: %s' % (sigma, roofit_sigma))


#---------------------------------------
# plot results
#---------------------------------------
# plot the gaussian data on a histogram
h = ROOT.TH1F("gaus", "gaus", 50, -3, 9)
for x in gaussian_data:
    h.Fill(x)
h = normalize(h)
    
# draw tgraph of fit line
fit_curve = norm(loc = roofit_mean, scale = roofit_sigma)
fit_graph = ROOT.TGraph()
for x in np.linspace(-3, 9, 50):
    fit_graph.SetPoint(fit_graph.GetN(), # set next point
                       x, # with this x
                       fit_curve.pdf(x)) # and this y
# add legend
leg = ROOT.TLegend()
leg.AddEntry(h, 'Gaussian Data', 'lep')
leg.AddEntry(fit_graph, 'Fit', 'l')

# plot on canvas
c = ROOT.TCanvas()
h.Draw()
fit_graph.Draw('l, same')
leg.Draw('same')
c.SaveAs("gaussian.png")
