TestArea=$PWD

if [ -d "MG5_aMC_v3_3_2" ]
  then
    echo "Madgraph directory found"
  else
    echo "Madgraph directory not found. Downloading Madgraph"
    wget https://launchpad.net/mg5amcnlo/3.0/3.3.x/+download/MG5_aMC_v3.3.2.tar.gz
    tar -zxf MG5_aMC_v3.3.2.tar.gz
    rm MG5_aMC_v3.3.2.tar.gz
    cd MG5_aMC_v3_3_2
    ./bin/mg5_aMC -f ../madgraph_packages.txt
    cd ../    
fi

export PYTHIA8=$TestArea/MG5_aMC_v3_3_2/HEPTools/pythia8
export PYTHIA8DATA=$TestArea/MG5_aMC_v3_3_2/HEPTools/pythia8/share/Pythia8/xmldoc

if [ -d "delphes" ]
  then
    echo "Directory Delphes already exists."
    echo "Setup Delphes environment"
    cd delphes
    source DelphesEnv.sh
    cd ../
  else
    echo "Delphes directory not found. Downloading"
    git clone https://github.com/delphes/delphes.git
    cd delphes
    make HAS_PYTHIA8=true
    cp ../DelphesEnv.sh .
    source DelphesEnv.sh
    cd $TestArea
fi
