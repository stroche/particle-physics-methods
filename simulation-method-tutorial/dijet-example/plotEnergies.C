#ifdef __CLING__
R__LOAD_LIBRARY(libDelphes)
#include "classes/DelphesClasses.h"
#include "external/ExRootAnalysis/ExRootTreeReader.h"
#endif

int eventNumber = 0; // pick the first event


void plotEnergies()
{
  gSystem->Load("libDelphes");

  TFile *f = new TFile("dijetOutput.root");
  TTree *t = (TTree*) f->Get("Delphes");

  ExRootTreeReader *treeReader = new ExRootTreeReader(t);
  TClonesArray *branchTower = treeReader->UseBranch("Tower");
  TClonesArray *branchJet = treeReader->UseBranch("Jet");
  TClonesArray *branchGenJet = treeReader->UseBranch("GenJet");

  
  TString name = "energies_"; name += eventNumber;
  TString title = "Dijet Tower Energies: #phi vs #eta Event "; title += eventNumber;

  // make a TH2F
  TH2F *etaPhi = new TH2F(name, title,
			  50, -4.9, 4.9,
			  50, -3.14159, 3.14159);

  // get the first event (because here we let eventNumber = 0)
  treeReader->ReadEntry(eventNumber);
  // fetch the number of calo towers in that event
  int nTowers = branchTower->GetEntries();

  // loop over all the calo towers
  // for each tower, fill the TH2F with the energy at the correct eta and phi
  for (int i=0; i < nTowers; ++i)
    {
      Tower *tower = (Tower*) branchTower->At(i);
      etaPhi->Fill(tower->Eta, tower->Phi, tower->E);		   
    }

  etaPhi->SetStats(0);
  etaPhi->GetXaxis()->SetTitle("#eta");
  etaPhi->GetYaxis()->SetTitle("#phi");
  etaPhi->GetZaxis()->SetTitle("E [Gev]");
     
  name += ".png";

  // also fetch the truth and reco jets, and plot them as circles
  std::vector<TEllipse*> jetCircles;
  std::vector<TEllipse*> genjetCircles;

  int nJets = branchJet->GetEntries();
  for (int i=0; i < nJets; ++i)
    {
      Jet *j = (Jet*) branchJet->At(i);
      TEllipse *el = new TEllipse(j->Eta, j->Phi, 0.4);
      el->SetFillStyle(0);
      el->SetLineColor(6);
      el->SetLineStyle(1);
      el->SetLineWidth(2);
      jetCircles.push_back(el);
    }
      
  int nGenJets = branchGenJet->GetEntries();
  for (int i=0; i < nGenJets; ++i)
    {
      Jet *j = (Jet*) branchGenJet->At(i);
      TEllipse *el = new TEllipse(j->Eta, j->Phi, 0.4);
      el->SetFillStyle(0);
      el->SetLineColor(2);
      el->SetLineStyle(1);
      el->SetLineWidth(2);
      genjetCircles.push_back(el);
    }
      

  TPaveText *txt = new TPaveText(0.05, 0.90, 0.2, 0.98, "NDC");
  TString jetCount = "Reco Jets: "; jetCount += nJets;
  txt->AddText(jetCount); ((TText*) txt->GetListOfLines()->Last())->SetTextColor(6);
  TString genjetCount = "Truth Jets: "; genjetCount += nGenJets;
  txt->AddText(genjetCount); ((TText*) txt->GetListOfLines()->Last())->SetTextColor(2);
      
  TString canvasName = "c"; canvasName += eventNumber;
  TCanvas *c = new TCanvas(canvasName, canvasName);
  etaPhi->Draw("COLZ");

  for (int i=0; i < jetCircles.size(); ++i)
    {
      jetCircles[i]->Draw("same");
    }
  for (int i=0; i < genjetCircles.size(); ++i)
    {
      genjetCircles[i]->Draw("same");
    }
  txt->Draw("same");
  c->SetRightMargin(0.15);
  c->SaveAs(name);
}

