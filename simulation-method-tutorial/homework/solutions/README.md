# Homework Solutions

## MadGraph

First, in MadGraph you'll do
```
generate p p > h j j, (h > b b~) $ w+ w- z
output madgraph-directory
launch
```

And set the run card and parameter card in accordance with the instructions. The correct configurations are located at [run_card.dat](run_card.dat) and [param_card.dat](param_card.dat)

Next, run
```bash
gunzip madgraph-directory/Events/run_01/unweighted_events.lhe.gz
```

## Pythia and Delphes

Then, make sure you're using the right [pythiaCard.cmnd](pythiaCard.cmnd) (this is just taken straight from the [dijet example](../../dijet-example/) with the only change as the number of events).

You'll need to tweak the CMS card a bit to follow the instructions. Specifically, set the pileup and change JetpTMin from 20 --> 25 in a few places. Eventually, it should look like [delphes_card_CMS_PileUp.tcl](delphes_card_CMS_PileUp.tcl)

To run Pythia and Delphes, you'll do the normal command:

```
path/to/delphes/DelphesPythia8 delphes_card_CMS_PileUp.tcl pythiaCard.cmnd output-file.root
```

which will produce *output-file.root*

## Analysis

Now for the fun part. If you get stuck, each step of the analysis is detailed wiht good comments in [analysis.C](analysis.C)

At the end, your plots should look something like:

![dijet-mass.png](dijet-mass.png)
![bb-mass.png](bb-mass.png)

You may notice in the plots that the mass peaks at around 100 GeV. We set the Higgs mass to 105 GeV, so this makes sense as we'd expect these to arise from the Higgs decay to bb.


And the output to the terminal should print something like:
```
Total number of events: 20000
Events with 4 jets: 8128
Events where both non-VBF jets are b-tagged: 751
Events where both non-VBF jets are b-tagged AND have a bottom quark in each jet radius: 658
```
