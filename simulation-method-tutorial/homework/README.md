# Homework

![VBF](vbf-feynman.png)

Your homework is as follows.

1. In madgraph, produce 20,000 VBF Higgs events, with the Higgs decaying to $`b \overline{b}`$. Set the Higgs mass to 105 GeV, and the bottom quark mass to 12 GeV in *param_card.dat*. You can change the number of events in *run_card.dat*. Also in *run_card.dat*, please force all the jets (bs included) to have a pT of at least 30 GeV and to be at least 0.5 away from each other. Don't forget to set decay_cuts to True!.

2. Run the hadronization and detector simulation using the pythia card from the last few examples (the one without the Z decay to taus). For the detector simulation, use the CMS card with pileup. Please set mean pileup to 30. Also please set it so that jets are reconstructed if they have a pT of at least 25 GeV.

3. Run some analysis:

   a. First, isolate events with exactly 4 reco jets.

   b. Take the jet pair with the highest invariant mass, mjj. This is your VBF pair.

   c. The remaining 2 jets should have come from the Higgs decay to bb. Plot their invariant mass. Tell me for what fraction of events are both of these jets b-tagged (in addition, plot the bb invariant mass for only these events). Using the *Particle* branch, can you confirm that if both jets are b-tagged, within each jet there is a bottom quark that came from a Higgs decay?