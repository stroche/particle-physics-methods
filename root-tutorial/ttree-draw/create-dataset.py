import ROOT
import numpy as np
from array import array

pT  = array('f', [0.0])
eta = array('f', [0.0])
phi = array('f', [0.0])


f = ROOT.TFile('toy-dataset.root', 'RECREATE')
t = ROOT.TTree('myTree', 'myTree')
t.Branch('pT',  pT,  'pT/F')
t.Branch('eta', eta, 'eta/F')
t.Branch('phi', phi, 'phi/F')

for i in range(10000):
    pT[0] = np.random.chisquare(4)*10.0
    eta[0] = np.random.normal(0, 2)
    phi[0] = np.random.uniform(-np.pi, np.pi)

    if abs(eta[0]) > 5.0:
        continue
    else:
        t.Fill()

t.Write()
f.Close()
