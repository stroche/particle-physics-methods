import ROOT
import numpy as np

#------------------------
# create random data that kinda looks like a Higgs mass distribution
#------------------------
# 100k events
mc_data_vals = np.random.normal(125, 3, 100000)

#------------------------
# plot that data
#------------------------

# start by making a 1d histogram that takes floats
# TH1F
# (T: how everything in ROOT starts)
# (H: histogram)
# (1: 1-d)
# (F: floating point inputs)

my_histogram = ROOT.TH1F("m_h", # histogram name. make sure this is unique
                         "Higgs Mass", # histogram title
                         100, # number of bins
                         100, # minimum value
                         150) # maximum value


# fill the histogram with all the data
for val in mc_data_vals:
    my_histogram.Fill(val)


#--------------------------
# set plotting options
#--------------------------
# x axis title will be higgs mass of that event
my_histogram.GetXaxis().SetTitle('m_{H} [GeV]')

# y axis title will be events per bin width
my_histogram.GetYaxis().SetTitle('Events / ' + str(round(50 / 100, 2)) + ' GeV')


# make line thick and red
my_histogram.SetLineWidth(2)
my_histogram.SetLineColor(ROOT.kRed - 6)

# make histogram filled blue
my_histogram.SetFillColor(ROOT.kBlue + 2)

# make it filled with a cool pattern
my_histogram.SetFillStyle(3144)


#----------------------------
# make a TLegend
#----------------------------
leg = ROOT.TLegend(0.12, # xmin
                   0.70, # ymin
                   0.44, # xmax
                   0.88) # ymax
leg.AddEntry(my_histogram,
             "Higgs Mass Plot", # label
             "f") # show fill


#----------------------------
# plot on TCanvas
#----------------------------
c1 = ROOT.TCanvas()
my_histogram.Draw()
leg.Draw("same")
c1.SaveAs("histogram.png")
