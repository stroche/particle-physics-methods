# Histogram Plotting

In particle physics, you are going to see **SO MANY** histograms. This is because quantum systems are messy, so we often collect lots of data.

Here, I show how to use ROOT to construct the following histogram. Multiple draw options are used for demonstration purposes.

![histogram](histogram.png)


Start by initializing the histogram

```python
my_histogram = ROOT.TH1F("m_h", # histogram name. make sure this is unique
                         "Higgs Mass", # histogram title
                         100, # number of bins
                         100, # minimum value
                         150) # maximum value

```

you fill a histogram with data by simply doing
```python
my_histogram.Fill(value)
```
for each of the data points.

## Plotting Options
One nice thing about ROOT is that you can control every aspect of your plots with nearly surgical precision

### Setting Titles

```python
my_histogram.SetTitle('Overhead title')
my_histogram.GetXaxis().SetTitle('X axis title')
my_histogram.GetYaxis().SetTitle('Y axis title')
```

### Set Title and Axis Tick Label Size
```python
my_histogram.SetTitleSize(0.2)
my_histogram.GetXaxis().SetTitleSize(0.05)
my_histogram.GetYaxis().SetTitleSize(0.05)

my_histogram.GetXaxis().SetLabelSize(0.03)
my_histogram.GetYaxis().SetLabelSize(0.03)
```


### Histogram Colors and Styles
```python
# make line thick and red
my_histogram.SetLineWidth(2)
my_histogram.SetLineColor(ROOT.kRed - 6)

# make histogram filled blue
my_histogram.SetFillColor(ROOT.kBlue + 2)

# make it filled with a cool pattern
my_histogram.SetFillStyle(3144)
```

### Axis Limits
You set the x axis limits when you initialized the histogram. The y axis limits can be adjusted with

```python
my_histogram.SetMinimum(100)
my_histogram.SetMaximum(5000)
```


### TLegend
You can make a legend like so
```python
leg = ROOT.TLegend(0.12, # xmin
                   0.70, # ymin
                   0.44, # xmax
                   0.88) # ymax
leg.AddEntry(my_histogram,
             "Higgs Mass Plot", # label
             "f") # show fill
```

## Drawing Stuff
To draw things, first you make a TCanvas, then you call .Draw() on all your objects you want to draw. The first one draws the axes, and for the rest you need to set the draw option "same" to keep it on the same canvas.

```python
c1 = ROOT.TCanvas()
my_histogram.Draw()
leg.Draw("same")
c1.SaveAs("histogram.png")
```

### Log scale
Using a logarithmic/linear scale is the responsibility of the TCanvas, not the histogram

```python
c1.SetLogy()
c1.SetLogx()
c1.Update()
```