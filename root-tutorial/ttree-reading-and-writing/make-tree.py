import ROOT
from array import array
import numpy as np

#-----------------------
# create fake data
#-----------------------
eta_data = np.random.uniform(-5, 5, 100000)
phi_data = np.random.uniform(-np.pi, np.pi, 100000)
pT_data  = np.random.exponential(1.2, 100000) * 50

#-----------------------
# create file with tree
#-----------------------

# create TFile
f = ROOT.TFile("my_file.root", "RECREATE")

# create TTree within that file
t = ROOT.TTree("ntuple", "ntuple")

# set up variables
pT = array('f', [0.0])
eta = array('f', [0.0])
phi = array('f', [0.0])

# link those variables to branches in the TTree
t.Branch('pT',  pT,  'pT/F')
t.Branch('eta', eta, 'eta/F')
t.Branch('phi', phi, 'phi/F')

# fill branches with fake data
for a,b,c in zip(pT_data, eta_data, phi_data):
    pT[0]  = a
    eta[0] = b
    phi[0] = c
    t.Fill()

# write to file
t.Write()
f.Close()
