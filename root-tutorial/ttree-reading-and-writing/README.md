# TTree Operations

## Writing
First, open a file
```python
import ROOT
f = ROOT.TFile("my_file.root", "RECREATE")
```
the *RECREATE* option will create the ROOT file even if one with the same name already exists

Next, create the TTree. We'll name ours "ntuple"
```python
t = ROOT.TTree("ntuple", "ntuple")
```


Next, set up your variables. This is a bit hacky in Python. Each variable is an array.array object of length 1.

```python
from array import array
pT = array('f', [0.0])
eta = array('f', [0.0])
phi = array('f', [0.0]) 
```
where *f* means it uses floating point values

Next, tie those variables to branches in the TTree that you'll create
```python
t.Branch('pT',  pT,  'pT/F')
t.Branch('eta', eta, 'eta/F')
t.Branch('phi', phi, 'phi/F')
```
The first one here is the name of the branch, the second is the associated variable. The final is the name of the branch again, followed by the datatype (here, *F* for float). This last one is not fully true, but more most purposes that's how it'll be used.

Next, fill the branches with data. For instnance, if I want 3 events in my tree, I can do as follows.

```python
# add first event
pT[0] = 12.2
eta[0] = 1.6
phi[0] = -1.1
t.Fill()

# add second event
pT[0] = 102.4
eta[0] = 0.5
phi[0] = 3.0
t.Fill()

# add third event
pT[0] = 55.4
eta[0] = -4.0
phi[0] = 0.2
t.Fill()
```

You can see, this is done by setting our variables tied to the branches, then calling *TTree::Fill*

Finally, write the TTree to the file and close it
```python
t.Write()
f.Close()
```


## Reading TTree

As before, open the TFile and fetch the associated TTree
```python
f = ROOT.TFile("my_file.root", "READ") # this time just use "READ" flag
t = f.Get("ntuple")
```

Again, set up variables to tie to branches
```python
# setup variables
pT  = array('f', [0.0])
eta = array('f', [0.0])
phi = array('f', [0.0])

# tie them to branches
t.SetBranchAddress("pT",  pT)
t.SetBranchAddress("eta", eta)
t.SetBranchAddress("phi", phi)
```

Now, when you call t.GetEntry(entryNumber), pT, eta, and phi will automatically be set to those entries. (Again, these are technically arrays of length 1 so it'll be the 0th index of everything.

Here, we'll calculate the average value of each branch:
```python
average_pT  = 0
average_eta = 0
average_phi = 0

for i in range(t.GetEntries()):
    t.GetEntry(i)
    average_pT  += pT[0]
    average_eta += eta[0]
    average_phi += phi[0]

average_pT  /= t.GetEntries()
average_eta /= t.GetEntries()
average_phi /= t.GetEntries()
```

As before, be sure to close the file, or bad things may happen
```python
f.Close()
```


## Bonus
It's a massive pain to try to turn a TBranch into a python array. I have a function that does it. It's in [useful-functions.py](useful-functions.py)