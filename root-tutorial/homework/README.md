# Homework

This homework will involve reading a ROOT file and plotting some stuff.

## Instructions
1. Run [get-dataset.py](get-dataset.py) to download the dataset and convert it to a ROOT file. The dataset is from [1712.10321](https://arxiv.org/abs/1712.10321)
and the conversion script was written by Ben Carlson and me for use in [2104.03408](https://arxiv.org/abs/2104.03408)

2. Explore the dataset with a TBrowser
```bash
$ root
   ------------------------------------------------------------------
  | Welcome to ROOT 6.26/00                        https://root.cern |
  | (c) 1995-2021, The ROOT Team; conception: R. Brun, F. Rademakers |
  | Built for linuxx8664gcc on Apr 10 2022, 16:50:00                 |
  | From heads/latest-stable@v6-26-00-319-g7663f82960                |
  | With c++ (Ubuntu 9.4.0-1ubuntu1~20.04.1) 9.4.0                   |
  | Try '.help', '.demo', '.license', '.credits', '.quit'/'.q'       |
   ------------------------------------------------------------------

root [0] TBrowser b
(TBrowser &) Name: Browser Title: ROOT Object Browser
root [1] 
```

What you see in the TBrowser will look something like this
![TBrowser screenshot](tbrowser-screenshot.png)

3. Plot the following:

   a. Electron, pion, and photon E0frac. Make the axis titles pretty, make sure the histograms are distinguishable, and put in a legend so I know which is which.
   
   b. TProfile of E0frac vs E0 for each of the three

   c. 2D histogram (TH2F) of lateralDepth vs showerDepth for photons

   d. Plot electron and pion E1, but normalize them to have a total area of 1 (unity-normalized). Put the plot on a logarithmic scale in the y axis

   e. Draw candle plots (also known as box and whisker plots in biology) comparing lateralWidth0, lateralWidth1, and lateralWidth2 across all 3 samples


# Solutions
The solutions are written in [homework-solutions.py](homework-solutions.py). The plots should look something like this

## a.

![homework-solution-plot-a.png](homework-solution-plot-a.png)

## b.

![homework-solution-plot-b.png](homework-solution-plot-b.png)

## c.

![homework-solution-plot-c.png](homework-solution-plot-c.png)

## d.

![homework-solution-plot-d.png](homework-solution-plot-d.png)

## e.

![homework-solution-plot-e.png](homework-solution-plot-e.png)

