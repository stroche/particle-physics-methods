import ROOT
from array import array
import numpy as np

#-------------------------
# get ROOT file and trees
#-------------------------
f = ROOT.TFile("calorimetry.root", "READ")
t_electron = f.Get("eplus")
t_photon = f.Get("gamma")
t_pion = f.Get("piplus")


#------------------------
# create variables and tie them to the trees
#------------------------
E0frac = array('f', [0.0])
E0 = array('f', [0.0])
lateralDepth = array('f', [0.0])
showerDepth = array('f', [0.0])
E1 = array('f', [0.0])
lateralWidth0 = array('f', [0.0])
lateralWidth1 = array('f', [0.0])
lateralWidth2 = array('f', [0.0])

# tie to electron tree
t_electron.SetBranchAddress("E0frac", E0frac)
t_electron.SetBranchAddress("E0", E0)
t_electron.SetBranchAddress("lateralDepth", lateralDepth)
t_electron.SetBranchAddress("showerDepth", showerDepth)
t_electron.SetBranchAddress("E1", E1)
t_electron.SetBranchAddress("lateralWidth0", lateralWidth0)
t_electron.SetBranchAddress("lateralWidth1", lateralWidth1)
t_electron.SetBranchAddress("lateralWidth2", lateralWidth2)

# tie to photon tree
t_photon.SetBranchAddress("E0frac", E0frac)
t_photon.SetBranchAddress("E0", E0)
t_photon.SetBranchAddress("lateralDepth", lateralDepth)
t_photon.SetBranchAddress("showerDepth", showerDepth)
t_photon.SetBranchAddress("E1", E1)
t_photon.SetBranchAddress("lateralWidth0", lateralWidth0)
t_photon.SetBranchAddress("lateralWidth1", lateralWidth1)
t_photon.SetBranchAddress("lateralWidth2", lateralWidth2)

# tie to pion tree
t_pion.SetBranchAddress("E0frac", E0frac)
t_pion.SetBranchAddress("E0", E0)
t_pion.SetBranchAddress("lateralDepth", lateralDepth)
t_pion.SetBranchAddress("showerDepth", showerDepth)
t_pion.SetBranchAddress("E1", E1)
t_pion.SetBranchAddress("lateralWidth0", lateralWidth0)
t_pion.SetBranchAddress("lateralWidth1", lateralWidth1)
t_pion.SetBranchAddress("lateralWidth2", lateralWidth2)


#-----------------
# a
#-----------------
# set up histograms
a_electron = ROOT.TH1F("a_electron", "a_electron",
                       60, 0.0, 0.6)
a_photon =  ROOT.TH1F("a_photon", "a_photon",
                       60, 0.0, 0.6)
a_pion =  ROOT.TH1F("a_pion", "a_pion",
                       60, 0.0, 0.6)

# fill with data
for i in range(t_electron.GetEntries()):
    t_electron.GetEntry(i)
    a_electron.Fill(E0frac[0])

for i in range(t_photon.GetEntries()):
    t_photon.GetEntry(i)
    a_photon.Fill(E0frac[0])

for i in range(t_pion.GetEntries()):
    t_pion.GetEntry(i)
    a_pion.Fill(E0frac[0])
    
# set draw options
a_electron.SetMaximum(26000)
a_electron.SetStats(0)
a_electron.SetLineColor(ROOT.kOrange+2)
a_electron.SetLineWidth(3)
a_electron.SetTitle("")
a_electron.GetXaxis().SetTitle("E_{0} / E")
a_electron.GetXaxis().SetTitleSize(0.05)
a_electron.GetYaxis().SetTitle("Events / %s" %round(0.6 / 60, 2))
a_electron.GetYaxis().SetTitleSize(0.05)

a_photon.SetLineColor(ROOT.kAzure-2)
a_photon.SetLineWidth(3)

a_pion.SetLineColor(ROOT.kGreen+1)
a_pion.SetLineWidth(3)

# create a legend
a_legend = ROOT.TLegend(0.6, 0.6, 0.7, 0.8)
a_legend.AddEntry(a_electron, "e^{+}", "l")
a_legend.AddEntry(a_photon, "#gamma", "l")
a_legend.AddEntry(a_pion, "#pi^{+}", "l")


# draw and save
a_canvas = ROOT.TCanvas("a_canvas", "a_canvas")
a_electron.Draw()
a_photon.Draw("same")
a_pion.Draw("same")
a_legend.Draw("same")
a_canvas.SetLeftMargin(0.15) # stop axis title from being pushed off
a_canvas.SetBottomMargin(0.11) # stop axis title from being pushed off
a_canvas.Update()
a_canvas.SaveAs("homework-solution-plot-a.png")

#----------------------
# b
#----------------------
b_electron = ROOT.TProfile("b_electron", "b_electron",
                           40, 0.0, 40e3,
                           0.0, 1.0)
b_photon = ROOT.TProfile("b_photon", "b_photon",
                           40, 0.0, 40e3,
                           0.0, 1.0)
b_pion = ROOT.TProfile("b_pion", "b_pion",
                       40, 0.0, 40e3,
                       0.0, 1.0)

# fill with data
for i in range(t_electron.GetEntries()):
    t_electron.GetEntry(i)
    b_electron.Fill(E0[0], E0frac[0])

for i in range(t_photon.GetEntries()):
    t_photon.GetEntry(i)
    b_photon.Fill(E0[0], E0frac[0])

for i in range(t_pion.GetEntries()):
    t_pion.GetEntry(i)
    b_pion.Fill(E0[0], E0frac[0])


# create a legend
# set draw options
b_electron.SetStats(0)
b_electron.SetLineColor(ROOT.kOrange+2)
b_electron.SetLineWidth(3)
b_electron.SetTitle("")
b_electron.GetXaxis().SetTitle("E_{0}")
b_electron.GetXaxis().SetTitleSize(0.05)
b_electron.GetYaxis().SetTitle("E_{0} / E")
b_electron.GetYaxis().SetTitleSize(0.05)

b_photon.SetLineColor(ROOT.kAzure-2)
b_photon.SetLineWidth(3)

b_pion.SetLineColor(ROOT.kGreen+1)
b_pion.SetLineWidth(3)

# create a legend
b_legend = ROOT.TLegend(0.6, 0.3, 0.7, 0.5)
b_legend.AddEntry(b_electron, "e^{+}", "l")
b_legend.AddEntry(b_photon, "#gamma", "l")
b_legend.AddEntry(b_pion, "#pi^{+}", "l")

# draw and save
b_canvas = ROOT.TCanvas("b_canvas", "b_canvas")
b_electron.Draw()
b_photon.Draw("same")
b_pion.Draw("same")
b_legend.Draw("same")
b_canvas.SetLeftMargin(0.15) # stop axis title from being pushed off
b_canvas.SetBottomMargin(0.11) # stop axis title from being pushed off
b_canvas.Update()
b_canvas.SaveAs("homework-solution-plot-b.png")

#--------------------
# c
#-------------------
c_photon = ROOT.TH2F("c_photon", "c_photon",
                     50, 0, 1.2,   # showerdepth
                     50, 0, 100e3) # lateraldepth

for i in range(t_photon.GetEntries()):
    t_photon.GetEntry(i)
    c_photon.Fill(showerDepth[0], lateralDepth[0])

# set draw options
c_photon.SetStats(0)
c_photon.SetTitle("")
c_photon.GetXaxis().SetTitle("Shower Depth")
c_photon.GetXaxis().SetTitleSize(0.05)
c_photon.GetYaxis().SetTitle("Lateral Depth")
c_photon.GetYaxis().SetTitleSize(0.05)
c_photon.GetZaxis().SetTitle("Events")
c_photon.GetZaxis().SetTitleSize(0.05)

ROOT.gStyle.SetPalette(ROOT.kIsland)

# draw and save
c_canvas = ROOT.TCanvas("c_canvas", "c_canvas")
c_photon.Draw("lego2z")
c_canvas.SetLeftMargin(0.11) # stop axis title from being pushed off
c_canvas.SetBottomMargin(0.11) # stop axis title from being pushed off
c_canvas.Update()
c_canvas.SaveAs("homework-solution-plot-c.png")

ROOT.gStyle.SetPalette(ROOT.kBird)

#----------------------
# d
#----------------------
# set up histograms
d_electron = ROOT.TH1F("d_electron", "d_electron",
                       50, 0.0, 100e3)
d_pion =  ROOT.TH1F("d_pion", "d_pion",
                       50, 0.0, 100e3)

# fill with data
for i in range(t_electron.GetEntries()):
    t_electron.GetEntry(i)
    d_electron.Fill(E1[0])

for i in range(t_pion.GetEntries()):
    t_pion.GetEntry(i)
    d_pion.Fill(E1[0])

# normalize
d_electron.Scale(1.0 / d_electron.Integral())
d_pion.Scale(1.0 / d_pion.Integral())
    
# set draw options
d_electron.SetMaximum(0.1)
d_electron.SetStats(0)
d_electron.SetLineColor(ROOT.kOrange+2)
d_electron.SetLineWidth(3)
d_electron.SetTitle("")
d_electron.GetXaxis().SetTitle("E_{1} [MeV]")
d_electron.GetXaxis().SetTitleSize(0.05)
d_electron.GetYaxis().SetTitle("Probability / %s MeV" %round(100e3 / 50))
d_electron.GetYaxis().SetTitleSize(0.05)

d_pion.SetLineColor(ROOT.kGreen+1)
d_pion.SetLineWidth(3)

# create a legend
d_legend = ROOT.TLegend(0.6, 0.6, 0.7, 0.8)
d_legend.AddEntry(d_electron, "e^{+}", "l")
d_legend.AddEntry(d_pion, "#pi^{+}", "l")


# draw and save
d_canvas = ROOT.TCanvas("d_canvas", "d_canvas")
d_electron.Draw()
d_pion.Draw("same")
d_legend.Draw("same")
d_canvas.SetLeftMargin(0.15) # stop axis title from being pushed off
d_canvas.SetBottomMargin(0.11) # stop axis title from being pushed off
d_canvas.Update()
d_canvas.SaveAs("homework-solution-plot-d.png")


#------------------------
# e
#------------------------
e_electron = ROOT.TH2F("e_electron", "e_electron",
                     3, 1, 3,   # bins
                       50, 0, 300) # lateralwidth

e_photon = ROOT.TH2F("e_photon", "e_photon",
                     3, 1, 3,   # bins
                     50, 0, 300) # lateralwidth

e_pion = ROOT.TH2F("e_pion", "e_pion",
                     3, 1, 3,   # bins
                     50, 0, 300) # lateralwidth


for i in range(t_electron.GetEntries()):
    t_electron.GetEntry(i)
    e_electron.Fill(1, lateralWidth0[0])
    e_electron.Fill(2, lateralWidth1[0])
    e_electron.Fill(2.9, lateralWidth2[0])


for i in range(t_photon.GetEntries()):
    t_photon.GetEntry(i)
    e_photon.Fill(1, lateralWidth0[0])
    e_photon.Fill(2, lateralWidth1[0])
    e_photon.Fill(2.9, lateralWidth2[0])


for i in range(t_pion.GetEntries()):
    t_pion.GetEntry(i)
    e_pion.Fill(1, lateralWidth0[0])
    e_pion.Fill(2, lateralWidth1[0])
    e_pion.Fill(2.9, lateralWidth2[0])

    
# set draw options
e_electron.GetXaxis().SetBinLabel(1, "Lateral Width 0")
e_electron.GetXaxis().SetBinLabel(2, "Lateral Width 1")
e_electron.GetXaxis().SetBinLabel(3, "Lateral Width 2")

e_photon.GetXaxis().SetBinLabel(1, "Lateral Width 0")
e_photon.GetXaxis().SetBinLabel(2, "Lateral Width 1")
e_photon.GetXaxis().SetBinLabel(3, "Lateral Width 2")

e_pion.GetXaxis().SetBinLabel(1, "Lateral Width 0")
e_pion.GetXaxis().SetBinLabel(2, "Lateral Width 1")
e_pion.GetXaxis().SetBinLabel(3, "Lateral Width 2")

e_electron.SetLineColor(ROOT.kOrange+2)
e_photon.SetLineColor(ROOT.kAzure-2)
e_pion.SetLineColor(ROOT.kGreen+1)

e_electron.SetStats(0)
e_photon.SetStats(0)
e_pion.SetStats(0)

e_electron.SetTitle("e^{+}")
e_photon.SetTitle("#gamma")
e_pion.SetTitle("#pi^{+}")

ROOT.gStyle.SetTitleSize(0.1, 't')

e_electron.GetXaxis().SetLabelSize(0.1)
e_photon.GetXaxis().SetLabelSize(0.1)
e_pion.GetXaxis().SetLabelSize(0.1)

e_electron.GetYaxis().SetLabelSize(0.06)
e_photon.GetYaxis().SetLabelSize(0.06)
e_pion.GetYaxis().SetLabelSize(0.06)


# draw and save
e_canvas = ROOT.TCanvas("e_canvas", "e_canvas")
p1 = ROOT.TPad("p1", "p1", 0, 0, 1, 0.3)
p2 = ROOT.TPad("p2", "p2", 0, 0.33, 1, 0.63)
p3 = ROOT.TPad("p3", "p3", 0, 0.67, 1, 0.97)
p1.Draw()
p2.Draw()
p3.Draw()

p1.cd()
e_electron.Draw("candle1")

p2.cd()
e_photon.Draw("candle1")

p3.cd()
e_pion.Draw("candle1")

e_canvas.Update()
e_canvas.SaveAs("homework-solution-plot-e.png")



# close the file
f.Close()
