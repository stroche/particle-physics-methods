import numpy as np
import ROOT

#----------------------------
# make data
#----------------------------
x_data = np.linspace(0, 3, 15)
y1_data = np.exp(x_data) + 5
y2_data = x_data**2

#---------------------------
# make tgraphs
#---------------------------
g1 = ROOT.TGraph(15)  # 1000 data points
g2 = ROOT.TGraph(15)  # 1000 data points

# fill graphs
for i in range(15):
    g1.SetPoint(i, x_data[i], y1_data[i])
    g2.SetPoint(i, x_data[i], y2_data[i])
    

#----------------------------
# set draw options
#----------------------------
g1.SetMarkerStyle(22)
g2.SetMarkerStyle(43)

g1.SetMarkerSize(2)
g2.SetMarkerSize(2.3)

g1.SetMarkerColor(34)
g2.SetMarkerColor(46)

g1.SetLineWidth(2)
g2.SetLineWidth(1)

g1.SetLineColor(30)
g2.SetLineColor(44)

# set g1 y limits for plotting
g1.GetHistogram().SetMinimum(-2)
g1.GetHistogram().SetMaximum(30)

# set g1 titles for plot
g1.GetXaxis().SetTitle("x")
g1.GetYaxis().SetTitle("y")
g1.SetTitle("A Sample TGraph")

#--------------------------
# create legend
#--------------------------
leg = ROOT.TLegend(0.22, 0.6, 0.5, 0.75)
leg.AddEntry(g1, "Graph 1", "pl") # fl gets the fill and the line
leg.AddEntry(g2, "Graph 2", "pl")



#---------------------------
# plot on canvas
#---------------------------
c = ROOT.TCanvas()
g1.Draw("alp") # draws axis, curve, fill, and point
g2.Draw("lp, same")
leg.Draw("same")
c.Update()
c.SaveAs("tgraphs.png")
