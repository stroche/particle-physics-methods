# TGraph Plotting

TGraphs are the y vs x unbinned graphs with which you're probably most familiar.

Here you'll see how to make them and what draw options are common.
![tgraphs](tgraphs.png)

## Creating TGraph
To initialize a graph wiht 20 points, do 
```python
my_graph = ROOT.TGraph(20)
```

To fill the points, you do as follows:
```python
for i in range(20):
    x = 3*i
    y = 2*i + 15
    my_graph.SetPoint(i, x, y)
```

If you don't know how many points are in the graph in advance, you can continuously add new ones like so.

```python
my_graph = TGraph()
my_graph.SetPoint(my_graph.GetN(), x, y)
```


## Draw Options
You can mess with the style of the markers and connecting lines (and other stuff)
```python
my_graph.SetMarkerStyle(22)
my_graph.SetMarkerSize(2)
my_graph.SetMarkerColor(34)
my_graph.SetLineWidth(2)
my_graph.SetLineColor(30)
```

## Axis limits
Axis titles and their sizes are set the same way as histograms.
The limits are set like

```python
# x limits
my_graph.GetXaxis().SetLimits(-1.2, 44)

# y limits
my_graph.GetHistogram().SetMinimum(-2)
my_graph.GetHistogram().SetMaximum(30)
```

## Plotting

Start by making a canvas
```python
c1 = ROOT.TCanvas()
```

To draw the axis, use the "a" draw option. I'll also add "l, and p" for the points and a connecting line.

```python
my_graph_1.Draw("alp")
my_graph_2.Draw("lp, same")
```