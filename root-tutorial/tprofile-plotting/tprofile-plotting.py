import ROOT
import numpy as np

#------------------------
# create fake data
#------------------------
x_data = np.random.uniform(-1.2, 3.3, 10000)
y_data = []
for x in x_data:
    y_data.append(np.random.normal(np.abs(np.sin(x)), x**2))
y_data = np.array(y_data)


#-------------------------
# make TProfile
#-------------------------
prof = ROOT.TProfile("prof", "prof",
                     25, -1.5, 3.5, # 25 bins in x from -15 --> 35
                     -30, 30, # yLow, yHigh
                     's') # use rms for error bars in y

for x,y in zip(x_data, y_data):
    prof.Fill(x,y) # fill with data


#-------------------------
# set draw options
#-------------------------
prof.SetStats(0)
prof.GetXaxis().SetTitle("X Value")
prof.GetYaxis().SetTitle("Binned Y Value")


#-------------------------
# plot on TCanvas
#-------------------------
c = ROOT.TCanvas()
prof.Draw()
c.SaveAs('tprofile-plot.png')
