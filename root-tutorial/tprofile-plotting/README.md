# Profile Plot

A TProfile is a weird combination of a histogram and a graph. It's binned along x, but instead of points, it gives the average value in that bin and the error.

![a profile plot](tprofile-plot.png)

## Creating TProfile
```python
prof = ROOT.TProfile("prof", "prof",
                     25, -1.5, 3.5, # 25 bins in x from -1.5 --> 3.5
                     -30, 30, # yLow, yHigh
                     's') # use rms for error bars in y
```

## Filling
You fill it, as you may have guessed with the prof.Fill(x,y), command for each data point

```python
for x,y in zip(x_data, y_data):
    prof.Fill(x,y)
```

## Draw Options and Plotting
The draw options are basically all the same for a histogram and TGraph where appropriate. Same with the plotting commands.