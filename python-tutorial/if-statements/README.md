# Conditional Statements

Sometimes you only want certain code to run if something else is the case. In these times, you'd use an if-then statement. This is easy to see with examples:

```python
x = 5
if x == 4:
    print('the variable x is equal to 4')
elif x == 5:
    print('the variable x is equal to 5')
else:
    print('x is not equal to 4 or 5')
```
because ```x == 5``` evaluates to true, this will return
```
the variable x is equal to 5!
```

## And
Sometimes you want to check if a few cases are true. Here, you can use *and* statements

```python
a = 4
b = 12

if (b % 2 == 0) and (b/a == 3):
    print('b is even, and b/a is 3')
elif (b % 2 == 0):
    print('b is even but b/a is not 3')
```

## Or
Also, sometimes you only care if one or the other case is true. In these times, use the *or* statement

```python
a = 4
b = 3
if (a % 2 == 0) or (b % 2 == 0):
    print('either a or b or both is/are even')
```
The statement above will print the message because although b is odd, a is even


Fun tip: when you need to combine and/or statements in sometimes confusing ways, look up DeMorgan's Laws! They can help simplify! For instance

```python
!(a or b)
```
is equivalent to 
```python
!a and !b
```

# Homework
You are conducting an experiment in which you're measuring Planck's constant, the defined value of which is
```math
h_{\text{lit}} = 6.62607015 \; \times 10^{-34} \frac{\text{m}^2 \, \text{kg}}{\text{s}}
```
Using the classic photoelectric effect experiment, you measure it to be
```math
h_{\text{exp}} = 6.60 \pm 0.01 \; \times 10^{-34} \frac{\text{m}^2 \, \text{kg}}{\text{s}}
```

How many standard deviations off from the defined value is your measurement? Using if/then statements, if this is within 1 standard deviation, print out "The error is acceptable". Otherwise, if you are within 2 standard deviations, print out "The error is probably unacceptable". If you are more than 2 standard deviations off, print out "The error is definitely unacceptable".