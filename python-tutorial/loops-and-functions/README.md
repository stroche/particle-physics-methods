# Loops and Functions

## Loops
In python, sometimes you want to do a bunch of related things. For instance, if you wanted to print all the numbers between 1 and 5, you could do

```python
print(1)
print(2)
print(3)
print(4)
print(5)
```

But this gets inconvenient quick. Let's say you wanted to print all the numbers between 1 and 1000. That would be annoying even with copy and paste. Loops help us solve this.

### For loop
The for loop in python helps you loop over a bunch of objects to do stuff. The best way to see how it works is by example

#### Example 1
Let's say you want to print all the numbers from 1-5 as before
```python
for i in range(1,6):
    print(i)
```

#### Example 2
Let's say you have a list of 5 numbers, and you want to make a new list, adding 2.5 to each value

```python
original_list = [1.2, 3.3, 4.4, 6.7]  # input list
output_list = []  # start with empty list
for x in original_list:
    output_list.append(x+2.5)
```

the output list is now *[2.7, 5.8, 6.9, 9.2]*


### While loop
The while loop continuously executes until it sees an end condition.

#### Example 1
To do our number printing example from before
```python
i = 1
while i <= 5:
      print(i)
      i += 1
```


#### Example 2
Example 1 could more easily be done with a for-loop. A while loop is more convenient when the stopping condition is less simple than incrementally counting. For instance, consider the famous Collatz Conjecture. In short, start at some number. If that number is even, divide it by 2. If it's odd, take 3n + 1. This has always been shown to decrease to 1 (though not yet proven). We can show this for n=334 with a while loop

```python
n = 334
while n != 1:
      print(n)
      if n%2 == 0:   # if even
      	 n = n / 2
      elif n%2 == 1: # if odd
      	 n = (3*n) + 1
```

Running that prints the sequence

[334, 167, 502, 251, 754, 377, 1132, 566, 283, 850, 425, 1276, 638, 319, 958, 479, 1438, 719, 2158, 1079, 3238, 1619, 4858, 2429, 7288, 3644, 1822, 911, 2734, 1367, 4102, 2051, 6154, 3077, 9232, 4616, 2308, 1154, 577, 1732, 866, 433, 1300, 650, 325, 976, 488, 244, 122, 61, 184, 92, 46, 23, 70, 35, 106, 53, 160, 80, 40, 20, 10, 5, 16, 8, 4, 2, 1]

which, sure enough, goes to 1 as the Collatz conjecture predicts.


## Functions

Along similar lines, sometimes you'll have blocks of code you'd like to re-run in different places. For instance, let's say I want to check the collatz conjecture for 335 now, or for all the numbers 1-1000. Re-running my code above 1000 times and changing n would be annoying. I could wrap it in another big loop iterating n from 1-1000, but that's also kind of annoying.

Functions allow you to solve this issue, by doing certain operations in easy blocks.

### Example 1
Let's say you often need to add 3 to numbers all over your code. A function that does that would look like

```python
def add_three(x):
    return x + 2
```

Note that here *x* is the input, and my function ends with a *return* statement, telling it what to spit out.

Now, I can do the following
```python
>>> my_number = 2
>>> my_other_number = add_three(my_number)
>>> print(my_other_number)
5
```

### Example 2
Let's turn our collatz conjecture from above into a function. Instead of printing the numbers along the path, let's count how many steps it takes to get from the starting number to 1. We'll return that

```python
def collatz(n):
    number_of_operations = 0
    while n != 1:
        if n%2 == 0:   # if even
            n = n / 2
        elif n%2 == 1: # if odd
            n = (3*n) + 1
        number_of_operations += 1
    return number_of_operations
```

Now I can get the number of steps it takes for convergence whenever I want in an easy, readable way.

```python
>>> print(collatz(4))
2
>>> print(collatz(15))
17
>>> print(collatz(67))
27
>>> print(collatz(1345))
114
```

# Homework
You are trying to solve the differential equation
```math
y'(x) - 7y(x) = x^2 + 2x + 3, \, \, \, \, y(0) = 6
```
for several values of x.
Unfortunately, you slept in during the day in differential equations when you learned how to solve this analytically. Thankfully, you remember that you can approximate a solution using Euler's method.

As a reminder, Euler's method states that for a differential equation
```math
y'(x) = f(x, y(x)), \, \, \, \, y(x_0) = y_0
```
you can iteratively approach the value of y by repeatedly calling:
```math
y_{n+1} = y_n + hy'_n(x_n) = y_n + hf(x_n, y_n(x))
```
```math
x_{n+1} = x_n + h
```
for some small step *h*.

For this homework:
1. Write a function that takes in *y0*, *x0*, *h*, and *x*, and uses Euler's method to solve for *y(x)* for the given function. Have it continuously apply the method until
```math
x_n = x
```

2. Use this function to find the value of y(x) for all values of x in: 
```python
x_vals = [1.3, 3.7, 2.2, 0.9, 2.1]
h = 0.0001
y0 = 6
x0 = 0
```

3. For the following starting conditions, *y(1.9) = 28.59087...* For the following values of h, how close to this do you get?
```python
h = [0.1, 0.05, 0.01, 0.001, 0.0001, 0.00001]
y0 = 0
x0 = 0
```