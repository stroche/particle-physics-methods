import numpy as np
from scipy import special, integrate, linalg

#---------------------
# Prob 1
#---------------------
print('Problem 1')

# using scipy.special.fresnel
z_vals = np.linspace(0, 5, 5)
fresnel_vals = np.array(special.fresnel(z_vals)).T # transpose it

# using scipy.integrate.quad
def fresnel_S_integrand(t):
    return np.sin(np.pi * (t**2) / 2)
def fresnel_C_integrand(t):
    return np.cos(np.pi * (t**2) / 2)

def fresnel_S(z):
    return integrate.quad(fresnel_S_integrand, 0, z)
def fresnel_C(z):
    return integrate.quad(fresnel_C_integrand, 0, z)

fresnel_vals_2 = []
for z in z_vals:
    fresnel_vals_2.append([fresnel_S(z),
                           fresnel_C(z)])
fresnel_vals_2 = np.array(fresnel_vals_2)

print('z', z)
print('Fresnel interals (method 1):', fresnel_vals)
print('Fresnel interals (method 2):', fresnel_vals_2)


#---------------------
# Prob 2
#---------------------
print('\n\nProblem 2')
x = np.linspace(0, 5, 5)
alpha = 2
y = special.jv(alpha, x)
print('alpha:', alpha)
print('x:', x)
print('y:', y)

#---------------------
# Prob 3
#---------------------
print('\n\nProblem 3')

def func(x,y):
    return 4*np.sin(x) - 4*x*y + 3*(x**2)*((y**4) - np.sin(x*y))

x_vals = np.array([-1, 1, 2, 5]).astype('float')
soln = integrate.solve_ivp(func,
                             t_span=(-1, 5),
                             y0=[0.0],
                             t_eval = x_vals)

print('x:', x_vals)
print('y:', soln.y)

#---------------------
# Prob 4
#---------------------
print('\n\nProblem 4')

R = np.array([[1, 1+1j, 2j],
              [1-1j, 5, -3],
              [-2j, -3, 0]])

r, psi = linalg.eig(R)
print('Eigenvalues:', r)
print('Eigenvector:', psi)

print('\n Eigenvalue %s corresponds to the eigenvector %s' %(r[-1], psi[:, -1]))
