# Classification

## TMVA Steps
Start by fetching the input file(s), and fetching necessary trees

```python
input_file = ROOT.TFile('moons.root', 'READ')  # read in the data file
signal_tree = input_file.Get('signal')  # identify the signal tree
background_tree = input_file.Get('background')  # identify the background tree
```

Next, create an output file that TMVA will store training information in
```python
output_file = ROOT.TFile('training-output.root', "RECREATE")
```

Then, create a TMVA *Factory* that will handle our training
```python
factory = ROOT.TMVA.Factory('TMVAClassification', output_file)  # setup a TMVA factory
```

Next, prepare a *Dataloader* with the input data. This includes signal and background trees, as well as necessary variables. If you only want to use some of the data, you can apply a cut

```python
dataloader = ROOT.TMVA.DataLoader('dataset')  # name our dataset 'dataset' because I'm uncreative

# add floating point x and y variables
dataloader.AddVariable('x', 'F')
dataloader.AddVariable('y', 'F')

# add signal and background trees
dataloader.AddSignalTree(signal_tree)
dataloader.AddBackgroundTree(background_tree)

cut = ROOT.TCut('')
```
Next, prepare the dataset
```python
dataloader.PrepareTrainingAndTestTree(cut, 'SplitMode=Random:NormMode=NumEvents')
```

Next, tell the factory that we'll be using a BDT for training. Give it the dataloader and some training parameters.

```python
factory.BookMethod(dataloader,
                   ROOT.TMVA.Types.kBDT,
                   'BDT',
                   'nTrees=100:maxDepth=4:BoostType=AdaBoost')
```

Finally, train and test the dataset
```python
factory.TrainAllMethods()
factory.TestAllMethods()
factory.EvaluateAllMethods()
output_file.Close()
```

## Classification Evaluation
There are a few ways to evaluate the performance of a classifier.

### Output Score Distributions
First, you can look at the output distributions. A classifier gives each event a *score* that says whether it is signal-like (usually closer to 1) or *background-like* (usually closer to 0 or -1). By plotting a histogram of the signal and background events at each score, you can see how the classifier did.

![output scores](output-score-distribution-example.png)

### ROC Curves
A receiver operating characteristic curve (ROC curve), is perhaps the most common plot you'll see in machine learning evaluation. Looking at the output score distribution, imagine that at each point on the x-axis, you counted the signal events to the right (signal acceptance), and background points to the left (background rejection). At each point, you can plot these. You're left with a ROC curve, where the upper right is the "ideal" case. This allows you to compare classifiers. 

![roc-example.png](roc-example.png)

There are often other variations of this that get shown. Personally I like background acceptance vs signal acceptance. Others like 1/ background acceptance vs signal acceptance and so on. I've seen a bunch of different variations. 

#### Important
The [ROOT::TMVA::ROCCurve](https://root.cern/doc/v614/classTMVA_1_1ROCCurve.html) class is very underpowered and not ideal for making different flavors of ROC curve or finding other information.

Because of this, I've made my own class that does much more. A copy is attached at [roc-class.py](roc-class.py) but I regularly update it in the fwX codebase.


### Efficiency Curves
One way to compare how a classifer's performance compares to one of the variables is to plot their *equal background rejection efficiencies*. This is a nontrivial plot, so let's go through the steps.

1. Pick a background rejection level

2. Find the classifier score that corresponds to that rejection. My ROCCurve class has a method that can do this task.

3. Plot a [TEfficiency](https://root.cern.ch/doc/master/classTEfficiency.html) for that cut.

#### Efficiency Example
I want to see how efficiency compares with the variable 'x' at 70% background rejection.

1. Pick 0.7 as background rejection (0.3 as background acceptance).

2. Using my ROCCurve class, use the method *get_mva_score_at_bkg_acc(self, target_bkg_acc)* to find the corresponding BDT score.

3. Create a TEfficiency with variable 'x' on the x axis.

4. Fill the TEfficiency with the points. The boolean pass value is simply whether the BDT score is greater than the threshold from point 2.

```python
x_vals = some array of x inputs for the events
y_vals = some array of y inputs for the events
truth_tags = array of boolean signal/background tags (true for signal, false for background)
bdt_scores = output bdt scores for the events

# create ROCCurve object of my class
roc_curve = ROCCurve(mva_targets=truth_tags,
	             scores=bdt_scores)

# get threshold
threshold = roc_curve.get_mva_score_at_bkg_acc(0.3) # 30% background acceptance

# create teff
eff = ROOT.TEfficiency("eff", "eff",
		       20, -5, 5) # 20 bins from -5 to 5

# fill teff with pass value and x value at each point
for x, score in zip(x_vals, bdt_scores):
    eff.Fill(score > threshold,
             x)
```


## Example

Do the following:
- run *classification-training.py*
- read the comments in *classification-training.py* to make sure you understand how it works

This will:
- create the dataset in moons.root (shown in moons.png)
- train a classification algorithm to separate signal from background

Do the following:
- look at *training-output.root* in a TBrowser
- in *TestTree*, *BDT* describes gives the BDT's guess on whether an event is signal-like (1) or background-like (-1)
- in *TestTree*, *classID* gives the true class, where 0 is signal and 1 is background (I know it's a stupid convention)
- in ROOT, type in *TMVA::TMVAGui("training-output.root");* and play around with the GUI. Especially look at *1*, *3*, *4a*, *4b*, *5a*, *5b*. What do these plots show?
- now go to [this website](http://tmva.sourceforge.net/old_site/optionRef.html#MVA::BDT) and see what values you can change in line 37 of *classification-training.py*. Can you make the regression better by messing with these? Try adding more trees, more depth, increasing nCuts, and changing BoostType

![moons.png](./moons.png)


## Homework
Go fetch the electron/photon/pion data from [root-tutorial](../../root-tutorial/homework). Using this dataset, can you isolate electrons from photons using a forest of boosted decision trees? Ben and I (and others) did it in [2104.03408](https://arxiv.org/abs/2104.03408).

Once you've trained your trees, do some analysis. Plot a few different flavors of ROC curve. Test out some different training configurations. Compare TEfficiencies for a few different BDTs, comparing vs *E0*. If you want to get fancy, try something besides a BDT! All the methods and their training parameters are located [here](http://tmva.sourceforge.net/old_site/optionRef.html).