# TMVA Tutorial

This tutorial explains how to use a forest of boosted decision trees to perform both classification and regression using [TMVA](https://root.cern/manual/tmva/)

# Types of machine learning
## Classification
Classification is the art of separating two "classes" of data given information about each. Consider separation of cats vs dogs based on pictures of them, or spam emails vs relevant ones.

In high energy physics, classification is used often to make quick determinations. Did that collision produce a Higgs boson? Was that an electron or a photon in our calorimeter?

[Classification Tutorial](classification)

## Regression
Regression is the art of finding an unknown variable value, knowing certain others. This is also known as *curve_fitting* sometimes. An example of regression in the real world is the weather channel estimating tomorrow's temperature given certain information about today.

In high energy physics, regression is often used to quickly determine the value of certain variables. For instance, estimating the energy of a muon in our detector given certain information about it's path such as in [this paper](https://arxiv.org/pdf/2201.06288.pdf)

[Regression Tutorial](regression)

# Training and testing
Most machine learning is done in two steps: training and testing

## Training
In training, known data is given to the algorithm to "teach" it what it's supposed to know. For instance, I could feed an algorithm 10,000 pictures of cats, and 10,000 pictures of dogs to let it find out what a cat looks like and what a dog looks like. In physics, this is often done with simulated data

## Testing
In testing, unknown events are evaluated by the classifier. In this stage, I might give the classifier a picture of my friend Izzy's pet, and ask it whether that animal is a dog or a cat. If the classifier has done a good job, then it will accurately tell me that my friend has a cat

# Boosted Decision Trees
This tutorial uses the Boosted Decision Tree method for classification and regression.

## Decision Tree
A decision tree makes "cuts" given input variables. When an input variable is put in, the event travels either left or right at each node until it ends with a classification of signal or background (cat or dog).
The figure below shows an example decision tree for determining whether an event is signal or background.

![BDT Example](bdt-example.png)

## Boosting
We can get better performance by using a *forest* of decision trees, and assigning each a weight based on how well it does. Then the weighted average can be taken.

![forest](forest.png)

## Training
A BDT is trained by testing a bunch of cuts and picking the one that best separates signal from background. This is recursively continued to produce a tree of decisions. The boost-weight is assigned by testing the final tree on some known events, and seeing how good it is at properly separating signal from background.

